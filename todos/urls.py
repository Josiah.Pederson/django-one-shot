from django.urls import path
from todos.views import (
    list_list_view, 
    list_detail_view, 
    list_create_view, 
    list_update_view,
    ListDeleteView,
    item_create_view,
    item_update_view,
)


urlpatterns = [
    path("", list_list_view, name="todo_list_list_url"),
    path("<int:pk>/", list_detail_view, name="list_detail_view_url"),
    path("create/", list_create_view, name="list_create_view_url"),
    path("<int:pk>/edit/", list_update_view, name="list_update_view_url"),
    path("<int:pk>/delete/", ListDeleteView.as_view(), name="list_delete_view_url"),
    path("items/create/", item_create_view, name="item_create_view_url"),
    path("items/<int:pk>/edit/", item_update_view, name="item_update_view_url"),
]